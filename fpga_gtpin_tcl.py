template = {
    "all": "proc gtpinproc {{ }} {{\nreturn [list {perbus} ] \n}}",
    "perbus": "{Pin}",
}


def perbus(pindict):
    return " ".join(
        sorted(
            [
                template["perbus"].format(**p)
                for k, p in pindict.items()
                if ("GT" in p["I_O_Type"] and "CLK" not in p["Pin_Name"])
            ]
        )
    )  # ,"H34","H35","K34","K35","M34","M35","P34","P35","V34","V35","T34","T35"]


def all(pindict):
    return template["all"].format(**(dict(perbus=perbus(pindict), pincount=len(pindict))))


def writefile(pindict, filename="fpga_gtpin.tcl"):
    with open(filename, "w") as f:
        f.write(all(pindict))
