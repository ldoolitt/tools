proc depsrc {filename {dir ./}} {
set fp [open ${filename} r]
set file_data [read $fp]
close $fp
set src []
set constrain []
set tcl []
set vh []
set unk []
set lib [dict create]
foreach line $file_data {
puts $line
if {[string index $line 0] eq "/"} {
	set dir ""
	puts "abs path:"
	puts $line
}
if [regexp {\.v\s*$} $line] {
		lappend src ${dir}$line
} elseif [regexp {\.sv\s*$} $line] {
		lappend src ${dir}$line
} elseif [regexp {\.vhd\s*$} $line] {
		lappend src ${dir}$line
} elseif [regexp {\.hex\s*$} $line] {
		lappend src ${dir}$line
} elseif [regexp {\.xdc\s*$} $line] {
		lappend constrain ${dir}$line
} elseif [regexp {\.tcl\s*$} $line] {
		lappend tcl ${dir}$line
} elseif [regexp {\.vh\s*$} $line] {
		lappend vh [file normalize ${dir}$line]
} elseif [regexp {\.lib\s*$} $line] {
		set dirlib $dir[file dirname $line]/
		set libname [file rootname [file tail $line]]
		set decret [depsrc ${dir}$line $dirlib]
		dict set lib $libname [lindex $decret 0]
} elseif [regexp {\.d\s*$} $line] {
		set dird ${dir}[file dirname $line]/
		set decret [depsrc ${dir}$line $dird]
		set srcd [lindex $decret 0]
		set constraind [lindex $decret 1]
		set tcld [lindex $decret 2]
		set vhd [lindex $decret 3]
		set unkd [lindex $decret 4]
		set libd [lindex $decret 5]
		set src [concat $src $srcd]
		set constrain [concat $constrain $constraind]
		set tcl [concat $tcl $tcld]
		set vh [concat $vh $vhd]
		set unk [concat $unk $unkd]
		set lib [dict merge $lib $libd]
} else {
		lappend unk $line
}
}
return [list $src $constrain $tcl $vh $unk $lib]
}

