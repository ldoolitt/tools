template = {
    "all": "proc pinbankproc {{ }} {{ \n {perbus}\n return $pinbankdict \n}}",
    "perbus": "dict set pinbankdict fpga_{pin} {Bank}",
}


def perbus(pindict):
    return "\n".join(sorted([template["perbus"].format(**p) for k, p in pindict.items()]))


def all(pindict):
    return template["all"].format(**(dict(perbus=perbus(pindict), pincount=len(pindict))))


def writefile(pindict, filename="fpga_pinbank.tcl"):
    with open(filename, "w") as f:
        f.write(all(pindict))
