import re
import sys


def sdict(strin):
    sdict = {}
    ssplit = re.split(r",|\n|\r", strin)
    for s1 in ssplit:
        s2 = s1.split()
        if len(s2) == 2:
            sdict[s2[1]] = s2[0]
    return sdict


if __name__ == "__main__":
    forig = open(sys.argv[1])
    sorig = forig.read()
    forig.close()
    fio = open(sys.argv[2])
    sio = fio.read()
    fio.close()
    dout = {}
    siod = sdict(sio)
    sorigd = sdict(sorig)
    soutlist = []
    for k, v in sorigd.items():
        if k in siod:
            v = siod[k]
        else:
            v = sorigd[k]
        dout[k] = v
        soutlist.append("%s %s" % (v, k))
    forig = open(sys.argv[1], "w")
    sorig = forig.write("\n,".join(soutlist))
    forig.close()
