proc synimpbit {outputdir target core} {
	puts [clock format [clock seconds] -format %Y%m%d_%H%M%S]
	puts symimpbit_no_proj
	if {[current_project -quiet] == ""} {
		open_project ${outputdir}/${target}.xpr
		set_property  top ${target} [current_fileset]
	}


	if {[info procs packagepinproc] == ""} {
		set packagepindictok false
		puts "no packagepindict"
	} else {
		set packagepindict [packagepinproc]
		set packagepindictok true
		puts "yes packagepindict"
	}

	if {[info procs pinbankproc] == "" } {
		set pinbankdictok false
		puts "no pinbankdict"
	} else {
		set pinbankdict [pinbankproc]
		set pinbankdictok true
		puts "yes pinbankdict"
	}
	if {[info procs bankiostandardproc] == ""} {
		set bankiostandardok false
		puts "no bankiostandard"
	} else {
		set bankiostandard [bankiostandardproc]
		set bankiostandardok true
		puts "yes bankiostandard"
	}
	launch_runs synth_1 -job ${core}
	wait_on_run synth_1
	puts synth_1_finish
	#write_checkpoint ./vivado_project/${target}/${target}.runs/synth_1/${target}_synth1.dcp
	if { $packagepindictok && $pinbankdictok&& $bankiostandardok } {
		open_run synth_1 -name synth_1
		puts synth_1_open
		packagepins $packagepindict
		iostandard $pinbankdict $bankiostandard
		puts "automatic iostandard, and pacagepin"
	}
	
	file mkdir ${outputdir}/${target}.runs/impl_1	
	write_checkpoint ${outputdir}/${target}.runs/impl_1/${target}_constrain.dcp
	opt_design
	place_design -directive Explore
	set WNS [ get_property SLACK [get_timing_paths -max_paths 1 -nworst 1 -setup] ]

	# Post Place PhysOpt Looping
	set NLOOPS 5 
	set TNS_PREV 0
	set WNS_SRCH_STR "WNS="
	set TNS_SRCH_STR "TNS="

	if {$WNS < 0.000} {

		for {set i 0} {$i < $NLOOPS} {incr i} {
			phys_opt_design -directive AggressiveExplore
			set WNS [ exec grep $WNS_SRCH_STR vivado.log | tail -1 | sed -n -e "s/^.*$WNS_SRCH_STR//p" | cut -d\  -f 1]                                    
			set TNS [ exec grep $TNS_SRCH_STR vivado.log | tail -1 | sed -n -e "s/^.*$TNS_SRCH_STR//p" | cut -d\  -f 1]
			if {($TNS == $TNS_PREV && $i > 0) || $WNS >= 0.000} {
				puts break1,TNS:$TNS,TNS_PREV:$TNS_PREV,i:$i,WNS:$WNS
				break
			}
			set TNS_PREV $TNS

			phys_opt_design -directive AggressiveFanoutOpt 
			set WNS [ exec grep $WNS_SRCH_STR vivado.log | tail -1 | sed -n -e "s/^.*$WNS_SRCH_STR//p" | cut -d\  -f 1]
			set TNS [ exec grep $TNS_SRCH_STR vivado.log | tail -1 | sed -n -e "s/^.*$TNS_SRCH_STR//p" | cut -d\  -f 1]
			if {($TNS == $TNS_PREV && $i > 0) || $WNS >= 0.000} {
				puts break2,TNS:$TNS,TNS_PREV:$TNS_PREV,i:$i,WNS:$WNS
				break
			}
			set TNS_PREV $TNS

			phys_opt_design -directive AlternateReplication
			set WNS [ exec grep $WNS_SRCH_STR vivado.log | tail -1 | sed -n -e "s/^.*$WNS_SRCH_STR//p" | cut -d\  -f 1]
			set TNS [ exec grep $TNS_SRCH_STR vivado.log | tail -1 | sed -n -e "s/^.*$TNS_SRCH_STR//p" | cut -d\  -f 1]
			if {($TNS == $TNS_PREV && $i > 0) || $WNS >= 0.000} {
				puts break3,TNS:$TNS,TNS_PREV:$TNS_PREV,i:$i,WNS:$WNS
				break
			}
			set TNS_PREV $TNS
		}
	}
	write_checkpoint ${outputdir}/${target}.runs/impl_1/${target}_place.dcp
	route_design -directive Explore
	route_design -tns_cleanup
	write_checkpoint ${outputdir}/${target}.runs/impl_1/${target}_impl1.dcp
	write_debug_probes -force ${target}.ltx
	write_bitstream -force -bin_file ./${target}.bit
#	write_bitstream -force ${target}.bin
	write_bitstream -force -bin_file ${outputdir}/${target}.runs/impl_1/${target}.bit
	open_checkpoint ${outputdir}/${target}.runs/impl_1/${target}_impl1.dcp
	write_hw_platform -fixed -include_bit -force -file ${outputdir}/${target}.xsa
	report_timing_summary -delay_type min_max -report_unconstrained -check_timing_verbose -max_paths 10 -input_pins -routable_nets -name timing_1
	puts [clock format [clock seconds] -format %Y%m%d_%H%M%S]
}
