proc proj {target part outputdir depd} {
puts ${target}
file mkdir ${outputdir}
create_project ${target} ${outputdir} -part ${part}  -force
set decret [depsrc ${depd}]
if [file exist xadcsim] {
	file copy -force xadcsim $outputdir/xadcsim
}
set src [lsort -unique [lindex $decret 0]]
set constrain [lindex $decret 1]
set tcl [lsort -unique [lindex $decret 2]]
set vh [lsort -unique [lindex $decret 3]]
set unk [lsort -unique [lindex $decret 4]]
set lib [lindex $decret 5]
set vhpath {}
foreach vhfile $vh {
	set vhfilepath [file  dirname $vhfile]
	if { $vhfilepath ni $vhpath } {
		lappend vhpath $vhfilepath
	}
}
#regsub -all "\n" $file_data " " file_data
puts constrain,$constrain
puts "tcl"
puts $tcl
puts "src"
puts $src
puts $constrain
puts lib=$lib
puts "vh"
puts $vh
if { 0 != [llength $src] } {
	add_files $src
}
if { 0 != [llength $vh ] } {
	add_files $vh
	add_files -fileset sim_1 -norecurse $vh
}
if { 0 != [llength $lib] } {
	dict for {libname filenames} $lib {
		add_files $filenames
		foreach filename $filenames {
			set_property library $libname [get_files $filename]
		}
	}
}
set_property include_dirs $vhpath [current_fileset]
if { 0 != [llength $constrain] } {
	add_files -fileset constrs_1 $constrain
}
foreach ip $tcl {
    source $ip
	puts "debug tcl"
	puts $ip
	puts [info vars]
}
set_property top $target [get_filesets sources_1]
puts "set top $target"
update_compile_order -fileset sources_1
}
